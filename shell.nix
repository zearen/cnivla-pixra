with import <nixpkgs> {};

mkShell {
  buildInputs = [
    (python3.withPackages(pkgs: with pkgs; [
      pillow
    ]))]
  ++
  (with pkgs; [
    harfbuzzFull
  ]);
}
