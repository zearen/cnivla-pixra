#! /usr/bin/env python3

import argparse
import os
import re

from PIL import Image, ImageDraw, ImageFont
import PIL.features

if not PIL.features.check_feature(feature='raqm'):
    raise Exception(
            'This script requires libraqm to properly render the ZLM.  Please'
            ' insure it\'s installed by following the instructions found '
            'here: https://pillow.readthedocs.io/en/stable/installation.html')


def parse_color(color_code, alpha=255):
    if not re.fullmatch('#[0-9A-Fa-f]{6}', color_code):
        raise Exception(
                ('Color code "{}" is not of form "#" followed by six hex ' +
                 'digits, e.g. "#A05c13"').format(color_code))
    return (int(color_code[1:3], 16),
            int(color_code[3:5], 16),
            int(color_code[5:7], 16),
            alpha)


parser = argparse.ArgumentParser(
        "Creates Zbalermorna attitudinals from a given font")
parser.add_argument(
        '--output_path', default='out',
        help='Directory to output images to.')
parser.add_argument(
        '--font_file', default='tanbo-regular.otf',
        help='Font used to write .uivla')
parser.add_argument(
        '--background_color', default='#42454a',
        help='The color used for the outline, preferably the chat background')
parser.add_argument(
        '--text_color', default='#bedbed',
        help='The color used for the text, preferably the same as the chat')
args = parser.parse_args()

back_color = parse_color(args.background_color)
text_color = parse_color(args.text_color)
if not os.path.exists(args.output_path):
    os.makedirs(args.output_path)

denpabu = '\ued89'
yhy = '\ued8a'
iy = '\uedaa'
uy = '\uedab'

py = '\ued80'
ty = '\ued81'
ky = '\ued82'
fy = '\ued83'
ly = '\ued84'
sy = '\ued85'
cy = '\ued86'
my = '\ued87'
xy = '\ued88'

by = '\ued90'
dy = '\ued91'
gy = '\ued92'
vy = '\ued93'
ry = '\ued94'
zy = '\ued95'
jy = '\ued96'
ny = '\ued97'

abu = '\ueda0'
ebu = '\ueda1'
ibu = '\ueda2'
obu = '\ueda3'
ubu = '\ueda4'
aibu = '\ueda6'
eibu = '\ueda7'
oibu = '\ueda8'
aubu = '\ueda9'

vowels_to_zlm = {'a': abu, 'e': ebu, 'i': ibu, 'o': obu, 'u': ubu}
diphthongs_to_zlm = {'ai': aibu, 'ei': eibu, 'oi': oibu, 'au': aubu}

valsi_name = []
# Start by adding the diphthongs:
valsi_name.extend((denpabu + zlm, latin)
                  for latin, zlm in diphthongs_to_zlm.items())
# Add in .iV and .uV
valsi_name.extend((iy + zlm, 'i' + latin)
                  for latin, zlm in vowels_to_zlm.items())
valsi_name.extend((uy + zlm, 'u' + latin)
                  for latin, zlm in vowels_to_zlm.items())
# Add in .V'V form attitudinals
valsi_name.extend((denpabu + zlm1 + yhy + zlm2, latin1 + 'h' + latin2)
                  for latin1, zlm1 in vowels_to_zlm.items()
                  for latin2, zlm2 in vowels_to_zlm.items())
# Add in ro'V
valsi_name.extend((ry + obu + yhy + zlm, 'roh' + latin)
                  for latin, zlm in vowels_to_zlm.items())
# Add in CUSTOM attitudinals here:
# valsi_name.append((iy + obu + yhy + obu + yhy + obu, 'iohoho'))
# valsi_name.append((iy + ibu + yhy + aubu, 'iihau'))
valsi_name.append((sy + aibu, 'sai'))
valsi_name.append((cy + aibu, 'cai'))
valsi_name.append((ny + aibu, 'nai'))
valsi_name.append((dy + aibu, 'dai'))
valsi_name.append((ry + ubu + yhy + ebu, 'ruhe'))
valsi_name.append((cy + ubu + yhy + ibu, 'cuhi'))
valsi_name.append((zy + obu + yhy + obu, 'zoho'))
valsi_name.append((xy + obu + yhy + obu, 'xoho'))
valsi_name.append((ly + abu + yhy + abu, 'laha'))
valsi_name.append((jy + ibu + yhy + abu, 'jiha'))
valsi_name.append((dy + ibu + yhy + aibu, 'dihai'))
valsi_name.append((fy + ibu + yhy + ibu, 'fihi'))
valsi_name.append((jy + ebu + yhy + ebu, 'jehe'))
valsi_name.append((vy + ibu + yhy + obu, 'viho'))
valsi_name.append((jy + ebu + yhy + ubu, 'jehu'))
valsi_name.append((cy + oibu, 'coi'))
valsi_name.append((cy + obu + yhy + obu, 'coho'))
valsi_name.append((fy + ibu + yhy + ibu, 'fihi'))
valsi_name.append((py + eibu, 'pei'))
valsi_name.append((ry + eibu, 'rei'))
valsi_name.append((py + ebu + yhy + ibu, 'pehi'))
valsi_name.append((ky + ibu + yhy + ebu, 'kihe'))
valsi_name.append((my + ibu + yhy + ubu, 'mihu'))
valsi_name.append((abu + yhy + oibu, 'ahoi'))
valsi_name.append((uy + abu + yhy + ubu, 'uahu'))
valsi_name.append((my + ibu + yhy + aubu, 'mihau'))


# Perhaps make arguments for this.
SIZE = 128, 128
WIDTH, HEIGHT = SIZE
HEIGHT_ADJUST = 7
FONT_SIZE = 112
STROKE_WIDTH = 4
zbalermorna = ImageFont.truetype(
        args.font_file, FONT_SIZE, layout_engine=ImageFont.LAYOUT_RAQM)


def draw_valsi(valsi, name):
    img = Image.new('RGBA', SIZE, (0, 0, 0, 0))
    canvas = ImageDraw.Draw(img)
    canvas.text((WIDTH // 2, HEIGHT // 2 + HEIGHT_ADJUST), valsi,
                font=zbalermorna, anchor='mm', fill=text_color,
                stroke_width=STROKE_WIDTH, stroke_fill=back_color)
    img.save(os.path.join(args.output_path, '{}.png'.format(name)), 'PNG')


for valsi, name in valsi_name:
    draw_valsi(valsi, name)
